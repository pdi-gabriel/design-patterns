# design-patterns
Repositório destinado para demonstração de conhecimento em relação a Design Patterns.


## Singleton

Singleton é um padrão de projeto que garante que uma classe tenha apenas uma instância, provendo um ponto de acesso global para essa instância.

Localizado em nosso sistema CCG, onde é realizado as conexões PDO.

Um exemplo é o conexaosabium.php, onde permite o instânciamento apenas uma vez, a partir do momento que é chamada pela primeira vez. Ou seja, se o controller "Estoque.php" instância o model ConexaoSabium utilizando o método getInstance(), quando outro controller precisa utilizar o model ConexaoSabium, ele também utiliza o método getInstance(), porém dessa vez apenas reutilizando a instância que já foi realizada.

Referências:

https://refactoring.guru/pt-br/design-patterns/singleton e José Adriano hehe.

## Factory

Factory é um padrão de projeto que fornece uma inteface para criar objetos em uma superclasse, porém permitindo que as superclasses alterem o tipo de objetos que serão criados. 

Localizado no sistema CCG, onde é realizado a criação dos xtypes, e também no Admin, com a criação de controllers.

Um exemplo é o Controller.php e ControllerTemplate.php, onde ambos são classes que tem a função de se estender por outras classes, importanto junto, seus métodos. Os métodos atríbuidos pelas demais classes podem ou não serem utilizados, depende da classe que for recebe-los decidir o que usar e como usar. Outro exemplo é a classe ControllerCadastroClientes, onde extende da classe ControllerTemplate, utilizando apenas o método  setContainer() e render(), e ainda decidindo qual view ele quer renderizar. Porém caso o controller que recebe a extensão não queira utilizar os métodos estendidos, ele pode apenas utilizar de seus próprios métodos.

Referências:

https://www.udemy.com/course/curso-design-patterns-java/ e https://refactoring.guru/pt-br/design-patterns/factory-method


## Prototype

Prototype é um padrão de projeto que permite copiar objetos existentes sem á suas classes específicas.

Não foi possível localizar um exemplo de Prototype nos nossos sistemas.

Um exemplo é a classe Carro.phpn onde tem a lógica de criar carros e destinarem eles ou para a garagem, ou para a venda em uma concessionária. Caso seja desenvolvido dois carros do mesmo modelo, porém apenas de cores diferentes, invés de recopiar os valores de seus atríbutos, é necessário apenas clonar um deles, e alterar apenas o atributo cor. Assim, um carro do mesmo modelo, mas com a cor do momento, vai para a venda, e o carro do mesmo modelo com a cor menos destaque no momento, vai para garagem. Para a clonagem, o objeto protótipo necessita precisam possuir os mesmos atributos, o que é possível, já que objetos da mesma classe podem acessar os atributos privados um do outro.

Referências:

https://refactoring.guru/pt-br/design-patterns/prototype/php/example#lang-features 
https://refactoring.guru/pt-br/design-patterns/prototype/php/example#lang-features
