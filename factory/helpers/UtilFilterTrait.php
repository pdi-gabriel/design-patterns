<?php

trait UtilFilterTrait {
    
    /**
     * remove caracteres nao a-zA-Z0-9_,. (espaco)
     * @param type $variable
     * @return type
     */
    public static function filterOrderBy ($variable) {
        return preg_replace('/[^\w,. ]+/', '', $variable);
    }
    
    public static function filterInput ($type, $variableName, $filter = FILTER_SANITIZE_FULL_SPECIAL_CHARS, $options = null) {
        return filter_input($type, $variableName, $filter, $options);
    }
    
    public static function filterVar ($variable, $variableName = null, $filter = FILTER_SANITIZE_FULL_SPECIAL_CHARS, $options = null) {
        if ($variableName !== null && !isset($variable[$variableName])) {
            return null;
        }
        
        $var = $variableName !== null ? $variable[$variableName] : $variable;
        
        if ($var !== null && is_array($var)) {
            $options |= FILTER_REQUIRE_ARRAY;
        }
        return filter_var($var, $filter,  $options);
    }

    public static function filterGet ($variableName = null, $filter = FILTER_SANITIZE_FULL_SPECIAL_CHARS, $options = null) {
        return self::filterVar($_GET, $variableName, $filter, $options);
    }

    public static function filterPost ($variableName = null, $filter = FILTER_SANITIZE_FULL_SPECIAL_CHARS, $options = null) {
        return self::filterVar($_POST, $variableName, $filter, $options);
    }

    public static function filterRequest ($variableName = null, $filter = FILTER_SANITIZE_FULL_SPECIAL_CHARS, $options = null) {
        return self::filterVar($_REQUEST, $variableName, $filter, $options);
    }

    public static function filterServer ($variableName = null, $filter = FILTER_SANITIZE_FULL_SPECIAL_CHARS, $options = null) {
        return self::filterVar($_SERVER, $variableName, $filter, $options);
    }
    
    public static function filterCookie ($variableName = null, $filter = FILTER_SANITIZE_FULL_SPECIAL_CHARS, $options = null) {
        return self::filterVar($_COOKIE, $variableName, $filter, $options);
    }

    /**
     * Com as fun��es de sanitiza��o agora � problem�tico alterar a vari�vel $_REQUEST diretamente,
     * pois na hora de pegar o valor o filterRequest tenta pegar do INPUT_GET e depois do INPUT_POST.
     * Aqui atualizamos as duas vari�veis se for necess�rio, e por fim atualizamos a vari�vel $_REQUEST.
     *
     * @param string $variableName
     * @param mixed $value
     * @return void
     */
    public static function setRequest ($variableName, $value) {
        if (self::filterGet($variableName)) {
            $_GET[$variableName] = $value;
        }
        
        if (self::filterPost($variableName)) {
            $_POST[$variableName] = $value;
        }

        $_REQUEST[$variableName] = $value;
    }
}