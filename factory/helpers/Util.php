<?php

class Util {
    use UtilFilterTrait;

    public static function redireciona($url) {
        header('Location:' . $url . '');
    }

    public static function redirecionaJs($url) {
        return 'window.location = "' . $url . '"';
    }

    public static function limpaForm($form) {
        return "$('" . $form . " input').val('');$('.form input:eq(0)').focus();";
    }

    public static function getExtensao($arquivo) {
        $strArquivo = explode(".", $arquivo);
        $strArquivo = array_reverse($strArquivo);
        $extensao = strtolower($strArquivo[0]);
        return $extensao;
    }

    public static function removeAcentos($texto) {

        return strtr($texto, '����������������������������������������������������', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
    }

    public static function mergeArray($array1, $array2) {

        foreach ($array1 as $chave => $valor) {

            if (isset($array2[$chave])) {

                $array1[$chave] = $array2[$chave];
            }
        }

        return $array1;
    }

    public static function minutosParaTempoDescricao($minutos) {

        $dias = 0;
        $horas = 0;

        while ($minutos >= 1440) {
            $dias += 1;
            $minutos -= 1440;
        }

        while ($minutos >= 60) {
            $horas += 1;
            $minutos -= 60;
        }

        $descricao = '';

        if ($dias > 0) {
            if ($dias == 1)
                $descricao .= '1 dia';
            else
                $descricao .= $dias . ' dias';
        }

        if ($horas > 0) {
            if ($horas == 1)
                $descricao .= '1 hora';
            else
                $descricao .= $horas . ' horas';
        }

        if ($minutos > 0) {
            if ($minutos == 1)
                $descricao .= '1 minuto';
            else
                $descricao .= $minutos . ' minutos';
        }

        return $descricao;
    }

    public function sobeImagemTamanhoVariavel($arquivo, $nome, $diretorio, $alturamaxima = 0, $larguramaxima = 0) {

        if (!preg_match("/\/(gif|bmp|png|jpg|jpeg|x\-shockwave\-flash){1}$/i", $arquivo["type"])) {



            throw new Exception('Extens�o do arquivo ' . $arquivo["name"] . ' inv�lida');
        } else {

            // Para verificar as dimens�es da imagem

            $tamanhos = getimagesize($arquivo["tmp_name"]);



            // Verifica largura

            if ($larguramaxima != 0 && $tamanhos[0] != $larguramaxima) {

                throw new Exception('Imagem ' . $arquivo["name"] . ' deve possuir Largura: ' . $larguramaxima . 'px.');
            } elseif ($alturamaxima != 0 && $tamanhos[1] != $alturamaxima) {

                throw new Exception('Imagem ' . $arquivo["name"] . ' deve possuir Altura: ' . $alturamaxima . 'px.');
            }
        }



        // Pega extens�o do arquivo

        preg_match("/\.(gif|bmp|png|jpg|jpeg|swf){1}$/i", $arquivo["name"], $ext);





        $imagem_nome = $nome . "." . $ext[1];





        $imagem_dir = $diretorio . $imagem_nome;



        // Faz o upload da imagem

        if (move_uploaded_file($arquivo["tmp_name"], $imagem_dir) == false)
            throw new Exception('Ocorreu um erro ao mover o arquivo ' . $arquivo["name"] . '.');


        $imagem = array('nome' => $imagem_nome, 'largura' => $tamanhos[0], 'altura' => $tamanhos[1]);

        return $imagem;
    }

    public function dataPorExtenso($data) {
        //Y-m-d
        setlocale(LC_ALL, NULL);
        setlocale(LC_ALL, 'pt_BR');
        return strftime('%d de %B de %Y', strtotime($data));
    }

    public static function limiteTexto($text, $len) {
        if (strlen($text) < $len) {
            return $text;
        }
        $text_words = explode(' ', $text);
        $out = null;


        foreach ($text_words as $word) {
            if ((strlen($word) > $len) && $out == null) {

                return substr($word, 0, $len) . "...";
            }
            if ((strlen($out) + strlen($word)) > $len) {
                return $out . "...";
            }
            $out .= " " . $word;
        }
        return $out;
    }

    public static function reloadModal() {
        Js::addEval("$('body').append('<div style=\"z-index:9998;opacity: 0.5;\" class=\"modal-backdrop fade in\"></div><div id=\"loading\" style=\"position:fixed;top:59px;z-index: 9999;top: 50%;left: 50%;margin-left: -64px;margin-top: -28px;\"><div class=\"alert\"><img style=\"padding-bottom: 2px;\" src=\"' + Admin.Config.PATH + 'img/loader3.gif\"> <strong style=\"font-family:Arial;font-size:12px;\">Aguarde...</strong></div></div>');");
        Js::addEval("location.reload(true);");
    }

    public static function scriptReloadModal() {
        return "$('body').append('<div style=\"z-index:9998;opacity: 0.5;\" class=\"modal-backdrop fade in\"></div><div id=\"loading\" style=\"position:fixed;top:59px;z-index: 9999;top: 50%;left: 50%;margin-left: -64px;margin-top: -28px;\"><div class=\"alert\"><img style=\"padding-bottom: 2px;\" src=\"' + Admin.Config.PATH + 'img/loader3.gif\"> <strong style=\"font-family:Arial;font-size:12px;\">Aguarde...</strong></div></div>');location.reload(true);";
    }

    public static function redirecionaModal($url) {
        Js::addEval("$('body').append('<div style=\"z-index:9998;opacity: 0.5;\" class=\"modal-backdrop fade in\"></div><div id=\"loading\" style=\"position:fixed;top:59px;z-index: 9999;top: 50%;left: 50%;margin-left: -64px;margin-top: -28px;\"><div class=\"alert\"><img style=\"padding-bottom: 2px;\" src=\"' + Admin.Config.PATH + 'img/loader3.gif\"> <strong style=\"font-family:Arial;font-size:12px;\">Aguarde...</strong></div></div>');");
        Js::addEval('window.location = "' . $url . '"');
    }

    public static function scriptRedirecionaModal($url) {
        return "$('body').append('<div style=\"z-index:9998;opacity: 0.5;\" class=\"modal-backdrop fade in\"></div><div id=\"loading\" style=\"position:fixed;top:59px;z-index: 9999;top: 50%;left: 50%;margin-left: -64px;margin-top: -28px;\"><div class=\"alert\"><img style=\"padding-bottom: 2px;\" src=\"' + Admin.Config.PATH + 'img/loader3.gif\"> <strong style=\"font-family:Arial;font-size:12px;\">Aguarde...</strong></div></div>');window.location = '" . $url . "';";
    }

    public static function utf8_encode_multidimensional_array($array) {

        if (count($array) > 0) {
            foreach ($array as $chave => $linhas) {
                foreach ($linhas as $chave_linha => $coluna) {
                    $array[$chave][$chave_linha] = utf8_encode($coluna);
                }
            }
        }

        return $array;
    }

    public static function utf8_decode_multidimensional_array($array) {

        if (count($array) > 0) {
            foreach ($array as $chave => $linhas) {
                foreach ($linhas as $chave_linha => $coluna) {
                    $array[$chave][$chave_linha] = utf8_decode($coluna);
                }
            }
        }

        return $array;
    }

    public static function utf8_encode_array($valor) {

        foreach ($valor as $chave_linha => $coluna) {
            $array[$chave_linha] = utf8_encode($coluna);
        }

        return $array;
    }

    public static function utf8_decode_array($valor) {

        foreach ($valor as $chave_linha => $coluna) {
            $array[$chave_linha] = utf8_decode($coluna);
        }

        return $array;
    }

    public static function numeroFilial($idfilial) {

        $multplicador = substr($idfilial, 0, 1);

        return $idfilial - ($multplicador * 10000);
    }

    /**
     * Formatando pre�o
     * @param float $value
     * @param string $tipo
     * @param boolean $logado
     * @return string
     */
    public static function formatPrice($value) {
        return number_format($value, 2, ',', '.');
    }

    public static function printDebug($array = array()) {
        if ($_SERVER['REMOTE_ADDR'] == '10.0.14.95' or $_SERVER['REMOTE_ADDR'] == '10.0.2.37') {
            echo '<pre>';
            var_dump($array);
            echo '</pre>';
        }
    }

    public static function dataPrimeiroDiaMes() {
        return '01-' . date('m-Y');
    }

    public static function dataUltimoDiaMes() {
        return cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y')) . '-' . date('m-Y');
    }

    public static function departamento($dpto, $tipo) {

        /*
         * dpto = array
         * 0 - Inicial
         * 9 - Final
         */

        $retorno = array();
        foreach ($dpto as $i) {
            $retorno[] = str_pad($i, 10, $tipo);
        }

        return( implode(",", $retorno) );
    }

    public static function getBrowser() {
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version = "";

        //First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        } elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }

        // Next get the name of the useragent yes separately and for good reason.
        if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        } elseif (preg_match('/Firefox/i', $u_agent)) {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        } elseif (preg_match('/Chrome/i', $u_agent)) {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        } elseif (preg_match('/Safari/i', $u_agent)) {
            $bname = 'Apple Safari';
            $ub = "Safari";
        } elseif (preg_match('/Opera/i', $u_agent)) {
            $bname = 'Opera';
            $ub = "Opera";
        } elseif (preg_match('/Netscape/i', $u_agent)) {
            $bname = 'Netscape';
            $ub = "Netscape";
        }

        // Finally get the correct version number.
        $known = array('Version', @$ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) .
                ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
            // we have no matching number just continue
        }

        // See how many we have.
        $i = count($matches['browser']);
        if ($i != 1) {
            //we will have two since we are not using 'other' argument yet
            //see if version is before or after the name
            if (strripos($u_agent, "Version") < strripos($u_agent, @$ub)) {
                $version = $matches['version'][0];
            } else {
                $version = $matches['version'][1];
            }
        } else {
            $version = $matches['version'][0];
        }

        // Check if we have a number.
        if ($version == null || $version == "") {
            $version = "?";
        }

        return array(
            'userAgent' => $u_agent,
            'name' => $bname,
            'version' => $version,
            'platform' => $platform,
            'pattern' => $pattern
        );
    }

    public static function utf8_decoder($array) {

        array_walk_recursive($array, function(&$item, $key) {
            if (mb_detect_encoding($item, 'utf-8', true)) {
                $item = utf8_decode($item);
            }
        });

        return $array;
    }

    public static function utf8_encoder($array) {

        array_walk_recursive($array, function(&$item, $key) {
            if (!mb_detect_encoding($item, 'utf-8', true)) {
                $item = utf8_encode($item);
            }
        });

        return $array;
    }

    public static function utf8_decode_recursive($objectOrArray) {
        return self::walk_recursive($objectOrArray, 'utf8_decode');
    }

    public static function utf8_encode_recursive($objectOrArray) {
        return self::walk_recursive($objectOrArray, 'utf8_encode');
//        return is_array($objectOrArray) || is_object($objectOrArray) ?
//                self::walk_recursive($objectOrArray, 'utf8_encode'):
//                json_encode($objectOrArray);
    }

    public static function walk_recursive($obj, $closure) {
        if (is_object($obj)) {
            $newObj = new \stdClass();
            foreach ($obj as $property => $value) {
                $newProperty = $closure($property);
                $newValue = self::walk_recursive($value, $closure);
                $newObj->$newProperty = $newValue;
            }
            return $newObj;
        } else if (is_array($obj)) {
            $newArray = array();
            foreach ($obj as $key => $value) {
                $key = $closure($key);
                $newArray[$key] = self::walk_recursive($value, $closure);
            }
            return $newArray;
        } else {
            return $closure($obj);
        }
    }

    public static function json_utf8_encoder($objectOrArray) {
        return json_encode(self::utf8_encode_recursive($objectOrArray));
    }

    public static function explode2D($string, $rowdelimiter = "\r\n", $columndelimiter = "\t") {
        return array_map(function($row) use ($columndelimiter) {
            return explode($columndelimiter, $row);
        }, explode($rowdelimiter, $string));
    }

    public static function valorPorExtenso($valor = 0, $bolExibirMoeda = true, $bolPalavraFeminina = false) {

        $valor = self::removerFormatacaoNumero($valor);

        $singular = null;
        $plural = null;

        if ($bolExibirMoeda) {
            $singular = array("centavo", "real", "mil", "milh�o", "bilh�o", "trilh�o", "quatrilh�o");
            $plural = array("centavos", "reais", "mil", "milh�es", "bilh�es", "trilh�es", "quatrilh�es");
        } else {
            $singular = array("", "", "mil", "milh�o", "bilh�o", "trilh�o", "quatrilh�o");
            $plural = array("", "", "mil", "milh�es", "bilh�es", "trilh�es", "quatrilh�es");
        }

        $c = array("", "cem", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos");
        $d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa");
        $d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezesete", "dezoito", "dezenove");
        $u = array("", "um", "dois", "tr�s", "quatro", "cinco", "seis", "sete", "oito", "nove");


        if ($bolPalavraFeminina) {

            if ($valor == 1) {
                $u = array("", "uma", "duas", "tr�s", "quatro", "cinco", "seis", "sete", "oito", "nove");
            } else {
                $u = array("", "um", "duas", "tr�s", "quatro", "cinco", "seis", "sete", "oito", "nove");
            }


            $c = array("", "cem", "duzentas", "trezentas", "quatrocentas", "quinhentas", "seiscentas", "setecentas", "oitocentas", "novecentas");
        }


        $z = 0;

        $valor = number_format($valor, 2, ".", ".");
        $inteiro = explode(".", $valor);

        for ($i = 0; $i < count($inteiro); $i++) {
            for ($ii = mb_strlen($inteiro[$i]); $ii < 3; $ii++) {
                $inteiro[$i] = "0" . $inteiro[$i];
            }
        }

        // $fim identifica onde que deve se dar jun��o de centenas por "e" ou por "," ;)
        $rt = null;
        $fim = count($inteiro) - ($inteiro[count($inteiro) - 1] > 0 ? 1 : 2);
        for ($i = 0; $i < count($inteiro); $i++) {
            $valor = $inteiro[$i];
            $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
            $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
            $ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

            $r = $rc . (($rc && ($rd || $ru)) ? " e " : "") . $rd . (($rd && $ru) ? " e " : "") . $ru;
            $t = count($inteiro) - 1 - $i;
            $r .= $r ? " " . ($valor > 1 ? $plural[$t] : $singular[$t]) : "";
            if ($valor == "000")
                $z++;
            elseif ($z > 0)
                $z--;

            if (($t == 1) && ($z > 0) && ($inteiro[0] > 0))
                $r .= ( ($z > 1) ? " de " : "") . $plural[$t];

            if ($r)
                $rt = $rt . ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? ", " : " e ") : " ") . $r;
        }

        $rt = mb_substr($rt, 1);

        return($rt ? ucfirst(strtolower(trim($rt))) : "zero");
    }

    public static function removerFormatacaoNumero($strNumero) {

        $strNumero = trim(str_replace("R$", null, $strNumero));

        $vetVirgula = explode(",", $strNumero);
        if (count($vetVirgula) == 1) {
            $acentos = array(".");
            $resultado = str_replace($acentos, "", $strNumero);
            return $resultado;
        } else if (count($vetVirgula) != 2) {
            return $strNumero;
        }

        $strNumero = $vetVirgula[0];
        $strDecimal = mb_substr($vetVirgula[1], 0, 2);

        $acentos = array(".");
        $resultado = str_replace($acentos, "", $strNumero);
        $resultado = $resultado . "." . $strDecimal;

        return $resultado;
    }

    public static function onlyNumber($str) {
        return preg_replace("/[^0-9]/", "", $str);
    }

    public static function onlyString($str) {
        return preg_replace("/[^A-Za-z�-��-�]/", "", $str);
    }


    /**
     * Valida��o de e-mail
     * @param string $email
     * @return string
     */
    public static function validacaoEmail($email) {
        return (is_bool(filter_var($email, FILTER_VALIDATE_EMAIL))) ? false : true;
    }

    /**
     * Remove coment�rios da String passada como par�metro e a retorna
     *
     * @param string $texto
     * @return string
     */
    public static function removeComentarios($texto)
    {
		$sqlComments = '@(([\'"]).*?[^\\\]\2)|((?:\#|--).*?$|/\*(?:[^/*]|/(?!\*)|\*(?!/)|(?R))*\*\/)\s*|(?<=;)\s+@ms';
		/* Commented version
		$sqlComments = '@
		    (([\'"]).*?[^\\\]\2) # $1 : Skip single & double quoted expressions
		    |(                   # $3 : Match comments
		        (?:\#|--).*?$    # - Single line comments
		        |                # - Multi line (nested) comments
		         /\*             #   . comment open marker
		            (?: [^/*]    #   . non comment-marker characters
		                |/(?!\*) #   . ! not a comment open
		                |\*(?!/) #   . ! not a comment close
		                |(?R)    #   . recursive case
		            )*           #   . repeat eventually
		        \*\/             #   . comment close marker
		    )\s*                 # Trim after comments
		    |(?<=;)\s+           # Trim after semi-colon
		    @msx';
		*/
		$uncommentedSQL = trim(preg_replace($sqlComments, '$1', $texto));

		return $uncommentedSQL;
	}


    /**
     * Get the client's IP addres
     *
     * Implementa��o segundo Zend Framework 1
     * https://github.com/komola/ZendFramework/blob/master/Controller/Request/Http.php#L1054
     *
     * @param  boolean $checkProxy
     * @return string
     */
    public static function getClientIp($checkProxy = false)
    {
        if ($checkProxy && isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif ($checkProxy && isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

    public static function moeda($get_valor) {
        $source = array('.', ',');
        $replace = array('', '.');
        $valor = str_replace($source, $replace, $get_valor);
        return $valor;
    }
}