<?php

class view2 {

    protected $_nome;
    protected $_html;

    function __construct($nome) {

        $this->_nome = $nome;

    }

    public function render(){

        $this->genFile();
        echo $this->_html;

    }

    public function setAttrib($chave, $valor){
        $this->{$chave} = $valor;
    }

    public function getHtml(){
        $this->genFile();
        return $this->_html;
    }

    protected function genFile(){

        ob_start();

        require_once $this->_nome.'.php';

        $this->_html = ob_get_contents();

        ob_end_clean();

    }

}

?>
