<?php

class view {

    protected $_nome;

    function __construct($nome) {
        $this->_nome = $nome;
    }

    public function render(){

        require_once dirname(__FILE__).DIRECTORY_SEPARATOR.$this->_nome.'.php';
        
    }

    public function setAttrib($chave, $valor){
        $this->{$chave} = $valor;
    }

}

?>
