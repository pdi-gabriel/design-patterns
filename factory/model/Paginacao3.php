<?php

class FuncaoGericoPaginacao {
     function filtrar($param)
     {
         
     }
}

class Paginacao3{

	public  $paginador = 'pag';
        public  $registrosporpagina = array(10,30,50,90);
	private $solicitador;
        protected $sql;
	protected $limite = 1;
	public  $quantidade = 3;
        private $conexao;

        private $ajax = false;


	// Construtor carrega a string usada para como paginador
	public function __construct($sql, $linhas, $conexao = null){
            
                if(isset($_GET['page'])){
                    $_REQUEST['pag'] = $_GET['page'];
                    unset($_REQUEST['page']);
                }
                
                $this->sql = 'select * from ('.$sql.')newsql where 1=1';

                $this->limite = $linhas;

                if($conexao === null)
                    $this->conexao = Conexao::getInstance();
                else
                    $this->conexao = $conexao;

		$this->solicitador = (isset($_REQUEST["{$this->paginador}"])) ? $_REQUEST["{$this->paginador}"] : 0 ;

                return $this;

	}

        public function enableAjax(){
            $this->ajax = true;
        }

        // Retorna o n�mero de resultados encontrados
	public function resultado(){
                if(!isset($this->numeroResultados)){
                    $query1 = strstr($this->sql, '*', true);
                    $query2 = strstr($this->sql, '*');

                    //file_put_contents('sql_paginacao2.sql', $query1.' count(*) '.substr($query2, 1));

                    $this->resultado = $this->conexao->query($query1.' count(*) '.substr($query2, 1));

                    $this->numeroResultados = $this->resultado->fetchColumn();
                }
		return $this->numeroResultados;
	}

	// Imprime um texto amig�vel mostrando o status das paginas em rela��o ao resultado atual
	public function imprimeBarraResultados(){
		if($this->resultado() > 0) {
			$retorno = 'Exibindo p�gina <span class="paginaatual">' . $this->paginaAtual()  . '</span> de <span class="totalpagina">' . $this->paginasTotais() . '</span> dispon�veis para <span class="linhasresultado">'.$this->resultado().'</span> resultados encontrados.';
		} else {
			$retorno = 'N�o foram encontrados resultados.';
		}

                return $retorno;

	}

	// Calcula o n�mero total de p�ginas
	public function paginasTotais(){
		$paginasTotais = ceil($this->resultado() / $this->limite);
		return $paginasTotais;
	}

	// Procura o n�mero da p�gina Atual
	public function paginaAtual(){

                if(!isset($this->paginaAtual)){
                    if (isset($this->solicitador) && is_numeric($this->solicitador)) {
                            $this->paginaAtual = (int) $this->solicitador;
                    } else {
                            $this->paginaAtual = 1;
                    }

                    if ($this->paginaAtual > $this->paginasTotais()) {
                            $this->paginaAtual = $this->paginasTotais();
                    }

                    if ($this->paginaAtual < 1) {
                            $this->paginaAtual = 1;
                    }
                }

		return $this->paginaAtual;

	}

	// Calcula o offset da consulta
	private function offset(){
		$offset = ($this->paginaAtual() - 1) * $this->limite;
		return $offset;
	}

	// Retorna o SQL para trabalhar posteriormente
	public function sql(){

                if(isset($this->orderby)){
                    return $this->sql." ".$this->orderby.  " LIMIT {$this->limite} OFFSET {$this->offset()} ";
                    unset($this->orderby);
                }

		$sql = $this->sql .  " LIMIT {$this->limite} OFFSET {$this->offset()} ";
		return $sql;
	}

	// Imprime a barra de navega��o da pagina��o
	public function imprimeBarraNavegacao(){

                $retorno = '';

                if($this->resultado() > 0) {

                        $retorno .= '<div class="pagination">';

                        $retorno .= '<ul>';
			if ($this->paginaAtual() > 1) {
				$retorno .= " <li><a href='?" . $this->paginador . "=1"  . $this->reconstruiQueryString($this->paginador) . "' class='linkpagina'>Primeira</a></li>";
				$anterior = $this->paginaAtual() - 1;
				$retorno .= " <a href='?" . $this->paginador . "=" . $anterior . $this->reconstruiQueryString($this->paginador) . "' class='linkpagina'>Anterior</a> ";
			}else{
                            $retorno .= " <li class='disabled'><a>Primeira</a></li>";
                            $retorno .= " <li class='disabled'><a>Anterior</a></li>";
                        }

			for ($x = ($this->paginaAtual() - $this->quantidade); $x < (($this->paginaAtual() + $this->quantidade) + 1); $x++) {
				if (($x > 0) && ($x <= $this->paginasTotais())) {
					if ($x == $this->paginaAtual()) {
						$retorno .= " <li class='disabled'><a>$x</a></li>";
					} else {
						$retorno .= " <a href='?" . $this->paginador . "=" . $x . $this->reconstruiQueryString($this->paginador) . "' class='linkpagina'>$x</a> ";
					}
				}
			}

			if ($this->paginaAtual() != $this->paginasTotais()) {
				$paginaProxima = $this->paginaAtual() + 1;
				$retorno .= " <a href='?" . $this->paginador . "=" . $paginaProxima . $this->reconstruiQueryString($this->paginador) . "' class='linkpagina'>Pr�xima</a> ";
				$retorno .= " <a href='?" . $this->paginador . "=" . $this->paginasTotais() . $this->reconstruiQueryString($this->paginador) . "' class='linkpagina'>�ltima</a> ";
			}else{
                                $retorno .= " <li class='disabled'><a>Pr�xima</a></li>";
				$retorno .= " <li class='disabled'><a>�ltima</a></li>";
                        }


			$retorno .= '</div></ul>';

                        if($this->ajax){

                        $retorno .= "<script>
                            $(document).ready(function(){
                                $('.linkpagina').live('click', paginar);
                            });

                            function paginar(e, pesquisa){

                                    

                                    if(window.ja_paginado != undefined)
                                        return false;

                                    if($(this).attr('ja_paginado') != undefined)
                                        return false;

                                    if(pesquisa == undefined){
                                        url = $(this).attr('href');
                                        $('#grid3').css('opacity', 0.5);
                                        $('.pagination ul').addClass('disabled');
                                        window.ja_paginado = true;
                                        $('a[class=\"linkpagina\"]').attr('ja_paginado', true);
                                        temp = $('<div />').load(url+' .table, .pagination', function(){
                                            $('#grid3').parent().html(temp.find('#grid3'));
                                            $('.pagination').parent().html(temp.find('.pagination'));
                                            delete window.ja_paginado;
                                        });

                                    }else{

                                        $('.pagination ul').addClass('disabled');
                                        $('a[class=\"linkpagina\"]').attr('ja_paginado', true);
                                        window.ja_paginado = true;
                                        $('#grid3').css('opacity', 0.5);

                                        temp = $('<div />').load(window.location.href+' #grid3, .pagination', $('form').serialize(),function(){
                                            $('#grid3').parent().html(temp.find('#grid3'));
                                            $('.pagination').parent().html(temp.find('.pagination'));
                                            delete window.ja_paginado;
                                        });

                                    }


                                    return false;
                                    e.preventDefault();

                            }
                        </script>";
                        }

                        return $retorno;

		}else{
                    $retorno = '';

                    $retorno .= '<div class="pagination" style="display:none;"></div>';

                    if($this->ajax){

                    $retorno .= "<script>
                            $(document).ready(function(){
                                $('.linkpagina').live('click', paginar);
                            });

                            function paginar(e, pesquisa){

                                    if(pesquisa == undefined){
                                        url = $(this).attr('href');
                                        $('#grid3').css('opacity', 0.5);
                                        $('#grid3').parent().load(url+' #grid3');
                                        $('.pagination').parent().load(url+' .pagination');
                                    }else{
                                        $('#grid3').css('opacity', 0.5);
                                        $('#grid3').parent().load(window.location.href+' #grid3', $('form').serialize());
                                        $('.pagination').parent().load(window.location.href+' .pagination', $('form').serialize());
                                    }


                                    return false;
                                    e.preventDefault();

                            }
                        </script>";

                    }

                    return $retorno;

                }
	}

	// Monta os valores da Query String novamente
	public function reconstruiQueryString($valoresQueryString) {
		if (!empty($_SERVER['QUERY_STRING'])) {
			$partes = explode("&", $_SERVER['QUERY_STRING']);
			$novasPartes = array();
			foreach ($partes as $val) {
				if (stristr($val, $valoresQueryString) == false)  {
					array_push($novasPartes, $val);
				}
			}
			if (count($novasPartes) != 0) {
				$queryString = "&".implode("&", $novasPartes);
			} else {
				return false;
			}
			return $queryString; // nova string criada
		} else {
			return false;
		}
	}

        public function where($coluna, $valor, $tipo = null){

            if(trim($valor) != ''){

                if($tipo==1){
                    //$this->sql .= " AND removeacentos(".$coluna.") = '".Util::removeAcentos($valor)."'";
                    $this->sql .= " AND (".$coluna.") = '$valor'";
                }else{
                    $valor = (int) $valor;
                    if(is_int($valor))
                        $this->sql .= " AND ".$coluna." = ".$valor;
                }

            }

            return $this;

        }
        
        public function maior($coluna, $valor){

            if(trim($valor) != ''){

                $this->sql .= " AND (".$coluna.") >= '$valor'";
              
            }

            return $this;
        }
        
        public function menor($coluna, $valor){

            if(trim($valor) != ''){

                $this->sql .= " AND (".$coluna.") <= '$valor'";
              
            }

            return $this;
        }

        public function containsArray($coluna, $valor, $tipo){

            if (is_array($valor)) {

                if (count($valor) > 0) {

                    if (trim(implode(',', $valor))) {

                        if ($tipo == PDO::PARAM_STR) {

                            foreach($valor as $chave => $item){
                                $valor[$chave] = "'".str_replace("'", "\'", $item)."'::varchar(255)";
                            }

                            $this->sql .= " AND " . $coluna . " @> ARRAY[" . implode(',', $valor) . "]";

                        } else {
                            //echo " AND ".$coluna." @> ARRAY[".  implode(',', $valor)."]";
                            $this->sql .= " AND " . $coluna . " @> ARRAY[" . implode(',', $valor) . "]";
                        }
                    }
                }
            }

            return $this;

        }
        
        public function arrayContains($coluna, $valor, $tipo){

            if (is_array($valor)) {

                if (count($valor) > 0) {

                    if (trim(implode(',', $valor))) {

                        if ($tipo == PDO::PARAM_STR) {

                            foreach($valor as $chave => $item){
                                $valor[$chave] = "'".str_replace("'", "\'", $item)."'::varchar(255)";
                            }

                            $this->sql .= " AND " . $coluna . " <@ ARRAY[" . implode(',', $valor) . "]";

                        } else {
                            //echo " AND ".$coluna." @> ARRAY[".  implode(',', $valor)."]";
                            $this->sql .= " AND " . $coluna . " <@ ARRAY[" . implode(',', $valor) . "]";
                        }
                    }
                }
            }

            return $this;

        }
        
        public function arrayEqual($coluna, $valor, $tipo){

            if (is_array($valor)) {

                if (count($valor) > 0) {

                    if (trim(implode(',', $valor))) {

                        if ($tipo == PDO::PARAM_STR) {

                            sort($valor);

                            foreach($valor as $chave => $item){
                                $valor[$chave] = "'".str_replace("'", "\'", $item)."'::varchar(255)";
                            }

                            $this->sql .= " AND " . $coluna . " = ARRAY[" . implode(',', $valor) . "]";

                        } else {
                            //echo " AND ".$coluna." @> ARRAY[".  implode(',', $valor)."]";
                            $this->sql .= " AND " . $coluna . " @> ARRAY[" . implode(',', $valor) . "]";
                        }
                    }
                }
            }

            return $this;

        }

        public function in($coluna, $arrayvalor, $tipo){

            if(count($arrayvalor) > 0 && (!(count($arrayvalor) == 1 && $arrayvalor[0] == ''))){

                if($tipo==PDO::PARAM_INT) {
                    $this->sql .= " AND ".$coluna." in(".implode(", ", $arrayvalor).")";
                } else {
                    foreach($arrayvalor as $valor){
                        $arrayvalores[] = "'".$valor."'";
                    }
                    $this->sql .= " AND ".$coluna." in(".implode(", ", $arrayvalores).")";
                }

            }

            return $this;

        }
        
        public function inText($coluna, $valor, $tipo){

            if($valor != ''){

                if($tipo==PDO::PARAM_INT) {
                    $this->sql .= " AND ".$coluna." in(".urldecode($valor).")";
                } else {
                    
                }

            }

            return $this;

        }


        public function ilike($coluna, $valor){

            if(trim($valor) != ''){

                $this->sql .= " AND ".$coluna." ilike('%".$valor."%') ";

            }

            return $this;

        }
        
        public function ilikeJsonb($coluna, $propriedade, $valor){

            if(trim($valor) != ''){

                $this->sql .= " AND exists(
                                        select 1 from jsonb_array_elements({$coluna}) where value->>'{$propriedade}' ilike '%{$valor}%'
                                ) ";

            }

            return $this;

        }
        
        public function ilikeInteiro($coluna, $valor){

            if(trim($valor) != ''){

                $this->sql .= " AND ".$coluna." ilike('".$valor."%') ";

            }

            return $this;

        }

        public function between($coluna, $valor1, $valor2){

            if(trim($valor1) != '' && trim($valor1) != ''){

                if(is_int($valor1))
                    $concat1 = $valor1;
                else
                    $concat1 = "'".$valor1."'";

                if(trim($valor2) == ""){
                    $concat2 = $concat1;
                }else{
                if(is_int($valor2))
                    $concat2 = $valor2;
                else
                    $concat2 = "'".$valor2."'";
                }

                $this->sql .= " AND (".$coluna." between ".$concat1." and ".$concat2.") ";
                
            }

            return $this;

        }

        public function orderBy($query){
            $this->orderby = " ORDER BY ".$query;
            return $this;
        }


        public function getRegistros(){

            //file_put_contents('sql_paginacao.sql', $this->sql());
            if(isset($_GET['orderBy']))
                $this->orderBy ($_GET['orderBy'].' '.$_GET['orderbyMethod']);

            $select = $this->conexao->query($this->sql());

            return $select->fetchAll(PDO::FETCH_ASSOC);

        }



}
