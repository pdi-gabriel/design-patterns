<?php


class ConexaoSabium extends PDO {

    private static $instancia;

    public function __construct() {

        $config_dir = dirname(__FILE__).DIRECTORY_SEPARATOR.'config.xml';


        if (!file_exists($config_dir)) {
            die('ERRO: O arquivo config.xml nao foi encontrado');
        }

        $cfg = simplexml_load_file($config_dir);
        
        $banco['host']    = strval($cfg->banco->exemplo->host);
        $banco['banco']   = strval($cfg->banco->exemplo->banco);
        $banco['usuario'] = strval($cfg->banco->exemplo->usuario);
        $banco['senha']   = strval($cfg->banco->exemplo->senha);

        parent::__construct("mysql:host=".$banco['host']. ";dbname=".$banco['banco']."", $banco['usuario'], $banco['senha']);

    }

    public static function getInstance()
    {
        if(!isset( self::$instancia )){
            try {
            	self::$instancia = new ConexaoSabium();
            } catch ( Exception $e ) {
              throw new PDOException($e->getMessage());
            }
        }
        return self::$instancia;
    }
}
?>
