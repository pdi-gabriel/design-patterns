<?php

class Controller {

    protected $_view;

    protected function setView($view){
        $this->_view = new view($view);
    }

    /**
     * Retorna Classe View
     * @return view Returns true on success or false on failure.
     */
    protected function getView(){
        return $this->_view;
    }

    protected function getRequest($chave){
        if(isset($_REQUEST[$chave]))
            return Util::filterRequest($chave);
        else
            return false;
    }

    protected function dataPostLATIN1(){
        foreach($_REQUEST as $chave => $valor) {
            if(is_array($valor)) {
                $_REQUEST[$chave] = utf8_decode(($valor));
            }
        }
        foreach($_POST as $chave => $valor) {
            if(!is_array($valor)) {
                $_POST[$chave] = utf8_decode(($valor));
            }
        }
    }


    protected function setViewNew($view){
        $this->_view = new view2($view);
    }

    /**
     * Retorna Classe View
     * @return view2 Returns true on success or false on failure.
     */
    protected function getViewNew(){
        return $this->_view;
    }


}

?>
