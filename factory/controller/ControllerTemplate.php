<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ControllerTab
 *
 * @author Macoto
 */
class ControllerTemplate extends Controller{
    
    protected $container;
    
    public function __construct() {
        
        $this->setViewNew('layout_template');
        $this->getViewNew()->js         = array();
        $this->getViewNew()->css        = array();
        
    }    
    
    protected function createView($nome){
        return new view2($nome);
    }
    
    protected function addJs($path_name){
        array_push($this->getViewNew()->js, $path_name);
    }
    
    protected function addCss($path_name){
        array_push($this->getViewNew()->css, $path_name);
    }
    
    protected function setContainer($nameview){
         
         $this->container = new view2($nameview);
         
    }
    
    protected function getContainer(){
         
         return $this->container;
         
    }
    
    protected function render(){
        
        $this->getViewNew()->container = $this->container->getHtml();
        $this->getViewNew()->render();
        die;
        
    }
    
    public function setJsView($viewportPATH){
        $this->container = new view2('layout_viewport');   
        $this->container->setAttrib('path', $viewportPATH);
    }
    
    public function setPanelJS($panelObject){
        $this->container = new view2('layout_extjs_panel');   
        $this->container->setAttrib('path', $panelObject);
    }
    
    public function __destruct() {
        
    }
    
}
