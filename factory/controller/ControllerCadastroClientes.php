<?php 

class ControllerCadastroClientes extends ControllerTemplate {

    public function index() {

        $this->setContainer('cadastro_clientes');
        $this->render();
    }

    public function carregarCliente() {
        
        $p = new Paginacao3("select 
                                p.cnpj_cpf,
                                p.nome
                            from 
                                glb.pessoa p"
                            , ConexaoSabium::getInstance());
        

        $array['data'] = Util::utf8_encode_multidimensional_array($p->getRegistros());

        $array['totalCount'] = $p->numeroResultados;

        echo json_encode($array);
    }


}