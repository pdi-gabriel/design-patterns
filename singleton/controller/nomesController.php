<?php
class NomesController {

    private $con;
    
    function __construct() {
        $this->con = ConexaoExemplo::getInstance();
    }


    function getNomesGrandes() {
        
        $prep = $this->con->prepare("SELECT * FROM nomes WHERE CHAR_LENGTH(nomes.nome) > 5");
        $prep->execute();

        $nomes = $prep->fetchAll(PDO::FETCH_ASSOC);

        return $nomes;

    }



}