<?php

class Carro
{
    public $cor;
    public $marca;
    public $modelo;

    public function __clone()
    {
        $this->cor =  $this->cor;
        $this->marca =  $this->marca;
        $this->modelo =  $this->modelo;
    }

    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * @return  self
     */ 
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;

        return $this;
    }

    public function getMarca()
    {
        return $this->marca;
    }

    /**
     *
     * @return  self
     */ 
    public function setMarca($marca)
    {
        $this->marca = $marca;

        return $this;
    }
 
    public function getCor()
    {
        return $this->cor;
    }

    /**
     *
     * @return  self
     */ 
    public function setCor($cor)
    {
        $this->cor = $cor;

        return $this;
    }

    

}


class DestinaCarro{


    function insereCarroGaragem(Carro $carro){
        
        echo "Cor: " . $carro->cor . " | Modelo: " . $carro->modelo . " | Marca: " . $carro->marca;

    }

    function insereCarroConcessionaria(Carro $carro){

        echo "Cor: " . $carro->cor. " | Modelo: " . $carro->modelo . " | Marca: " . $carro->marca;;

    }


}


function teste()
{
    $car1 = new Carro();
    $car1->setCor('vermelho');
    $car1->setMarca("Fiat");
    $car1->setModelo("Uno");

    $car2 = clone $car1;
    $car2->setCor('azul');

    $destino = new DestinaCarro;
    $destino->insereCarroConcessionaria($car1);
    echo "<br>";
    $destino->insereCarroGaragem($car2);
    

    
}

teste();